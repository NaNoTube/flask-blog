from wtforms import Form, BooleanField, TextField, PasswordField, validators, FileField, TextAreaField

class RegistrationForm(Form):
    username = TextField('username', [validators.Length(min=4, max=30)])
    email = TextField('Email Address', [validators.Length(min=6, max=35)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Password must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the TOS', [validators.Required()])

class LoginForm(Form):
    username = TextField('username', [validators.Length(min=4, max=30)])
    password = PasswordField('password', [validators.Required()])

class PostForm(Form):
    title = TextField('title')
    text = TextField('text')
    image = FileField('image')
#class ImageForm(Form):
#    image = FileField('image')

class CommentForm(Form):
    comment = TextAreaField('comment')
