from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from database import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column('username', String(30), nullable=True, unique=True)
    email = Column('email', String(128), nullable=True)
    password = Column('password', String(128), nullable=True)

    def __init__(self, username=None, email=None, password=None):
        self.username = username.lower()
        self.email = email.lower()
        self.password = password.lower()

    def __repr__(self):
        return '%s' % self.username.encode('utf-8')

class Post(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    writer = Column('user', String, nullable=False)
#    writer_id = Column(Integer, ForeignKey('users.id'))
    title = Column('title', String(128), nullable=False)
    text = Column('text', String, nullable=False)
    time = Column('time', String, nullable=False)
    image = Column('image', String, nullable=True)

    def __init__(self, writer=None, title=None, text=None, time=None, image=None):
        self.writer = writer
        self.title = title
        self.text = text
        self.time = time
        self.image = image

    def __repr__(self):
        return '%s' % self.title.encode('utf-8')

class Comment(Base):
    __tablename__ = 'comments'
    id = Column(Integer, primary_key=True)
    post_id = Column(Integer, ForeignKey('posts.id'), nullable=False)
    writer = Column('user', String, nullable=False)
    comment = Column('comment', String, nullable=False)
    time = Column('time', String, nullable=True)

    def __init__(self, post_id=None, writer=None, comment=None, time=None):
        self.post_id = post_id
        self.writer = writer
        self.comment = comment
        self.time = time

    def __repr__(self):
        return '%s' % self.comment.encode('utf-8')
