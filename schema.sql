drop table if exists users;
create table users (
    id integer primary key autoincrement,
    username string not null,
    email string not null,
    password string not null
);

drop table if exists posts;
create table posts (
    users_id REFERENCES users (id),
    users_name REFERENCES users (username),
    id integer primary key autoincrement,
    title string not null,
    text string not null,
    time string not null,
    image string
);

drop table if exists comments;
create table comments (
    users_id REFERENCES users (id),
    writer REFERENCES users (username),
    id integer primary key autoincrement,
    comment string not null
);
