import os
from contextlib import closing
import sqlite3
from flask import (Flask, request, session, g, redirect, url_for, abort,
    render_template, flash, send_from_directory, jsonify)
from database import db_session
from models import User, Post, Comment
from forms import RegistrationForm, LoginForm, PostForm, CommentForm
from time import localtime
from werkzeug import secure_filename, url_decode
DATABASE = '/tmp/blog.db'
DEBUG = True
SECRET_KEY = 'development key'
UPLOAD_FOLDER = './images/'
ALLOWED_EXTENSIONS = set(['jpg', 'jpeg', 'png', 'gif'])

app = Flask(__name__)
app.config.from_object(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

class MethodRewriteMiddleware(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if 'METHOD_OVERRIDE' in environ.get('QUERY_STRING', ''):
            args = url_decode(environ['QUERY_STRING'])
            method = args.get('__METHOD_OVERRIDE__')
            if method:
                method = method.encode('ascii', 'replace')
                environ['REQUEST_METHOD'] = method
        return self.app(environ, start_response)

@app.before_request
def before_requset():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    g.db.close()

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET'])
def bootstrap():
    login_form = LoginForm(request.form)
    return render_template('bootstrap.html', login_form=login_form)

@app.route('/posts/', methods=['GET'])
def blog():
        cur = g.db.execute('select id, title, text from posts order by id desc')
        entries = [dict(id=row[0], title=row[1], text=row[2]) for row in cur.fetchall()]
        return render_template('show_entries.html', entries=entries)

@app.route('/posts/', methods=['POST'])
def add_post():
    if not session.get('logged_in'):
        print "not logged_in"
        abort(401)
    if request.method == 'POST':
        post_form = PostForm(request.form)
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        post = Post(session['username'], post_form.title.data, post_form.text.data, "21:31", file.filename)
        db_session.add(post)
        db_session.commit()
        test_data = {
            "title" : post_form.title.data,
            "text" : post_form.text.data
                     } 
        print jsonify(test_data).data
        print post_form.title.data, post_form.text.data 
    return redirect(url_for('blog'))

@app.route('/posts/new', methods=['GET'])
def post_form():
    return render_template('add_post.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    login_form = LoginForm(request.form)
    register_form = RegistrationForm(request.form)
    if request.method == 'POST' and register_form.validate():
        user = User(register_form.username.data, register_form.email.data,
            register_form.password.data)
        db_session.add(user)
        db_session.commit()
        print user.username, user.email, user.password
        print User.query.all()
        return redirect(url_for('login'))
    return render_template('register.html', register_form=register_form, login_form=login_form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    login_form = LoginForm(request.form)
    input_username = unicode(login_form.username.data)
    input_password = unicode(login_form.password.data)
    if request.method == 'POST':
        db_username = unicode(User.query.filter(
            User.username == login_form.username.data
        ).first())
        db_password = User.query.filter(
            User.username == login_form.username.data
        ).first().password
        if input_username != db_username:
            error = 'Invalid username'
            print "invalid username"
        elif input_password != db_password:
            error = 'Invalid password'
            print "invalid password"
        else:
            session['logged_in'] = True
            session['username'] = input_username
            print session['username']
            print "you are logged in"
            return redirect(url_for('blog'))
    return render_template('logintest.html', login_form=login_form)

@app.route('/posts/<post_id>/edit')
def modify_post(post_id):
    original_post = Post.query.filter(Post.id == post_id).first()
    post = Post(session['username'], original_post.title, original_post.text, "21:23")
    if original_post.title and original_post.text:
        return render_template('modify.html', post=post, post_id=post_id)
    else: 
        return "None"

@app.route('/posts/<post_id>', methods=['GET', 'POST'])
def add_comment(post_id):
    comment_form = CommentForm(request.form)
    comment_data = request.data
    ret_data = { 
        "value" : request.data,
        "user_name" : session['username'],
        "filename" : Post.query.filter(Post.id == post_id).first().image
    }
    cur = g.db.execute('select title, text from posts where id = ' + post_id) 
    comment = g.db.execute('select user, comment from comments where post_id = '+ post_id)
    filename = Post.query.filter(Post.id == post_id).first().image
    comments = [dict(user=sow[0], comment=sow[1]) for sow in comment.fetchall()]
    entries = [dict(title=row[0], text=row[1]) for row in cur.fetchall()]
    if request.method == 'POST':
        comment = Comment(post_id, session['username'], comment_data, "1:2")
        db_session.add(comment)
        db_session.commit()
        print jsonify(ret_data).data
        return jsonify(ret_data)

    return render_template('view_post.html', entries=entries, comment_form=comment_form, comments=comments, post_id=post_id, filename=filename)

@app.route('/posts/<post_id>/update', methods=['POST'])
    # TODO: change uri to posts/<post_id>
def update_post(post_id):
    post = Post.query.filter(Post.id == post_id).first()
    post.title = request.form['title']
    post.text = request.form['text']
    db_session.commit()
    return redirect(url_for('blog', post_id=post_id))

@app.route('/images/<filename>')
def view_image(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route('/logout', methods=['POST'])
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('bootstrap'))

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

if __name__ == '__main__':
    app.run()
